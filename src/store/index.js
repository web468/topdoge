import React, { useEffect, useState } from "react";
import * as solanaWeb3 from "@solana/web3.js";
const Mycontext = React.createContext();

export const Myprovider = (props) => {
  const [phantom, setPhantom] = useState(window["solana"]);
  const [filterdogs, setfilterdogs] = useState([]);
  const [filtertext, setfiltertext] = useState("");
  const [alldoges, setalldoges] = useState([]);
  const [showWallet, setshowWallet] = useState(false);
  const [walletAddress, setwalletAddress] = useState("");
  const [walletprice, setwalletwalletprice] = useState(null);

  useEffect(() => {
    if (window["solana"]?.isPhantom) {
      setPhantom(window["solana"]);
    }
  }, [phantom]);
  const handleLoad = () => {
    if (window["solana"]?.isPhantom) {
      setPhantom(window["solana"]);
    }
  };
  window.addEventListener("load", handleLoad);
  const connectHandler = async () => {
    await phantom.connect();
    if (phantom.isConnected == true) {
      setshowWallet(true);
      const wallet = phantom.publicKey.toString();
      setwalletAddress(wallet);
      var connection = new solanaWeb3.Connection(
        solanaWeb3.clusterApiUrl("devnet"),
        "confirmed"
      );
      const balance = await connection.getBalance(phantom.publicKey);
      setwalletwalletprice((balance / 1000000000).toFixed(2).toString());
    }
  };
  const disconnectHandler = () => {
    phantom.disconnect();
    setPhantom(window["solana"]);
    setshowWallet(false);
  };

  useEffect(() => {
    const fetching = async () => {
      fetch("./dogs.json")
        .then((response) => {
          return response;
        })
        .then((data) => {
          // Work with JSON data here
          setfilterdogs(Object.values(data));
          setalldoges(Object.values(data));
        })
        .catch((err) => {
          // Do something for an error here
          console.log("Error Reading data " + err);
        });
    };
    fetching();
  }, []);

  useEffect(() => {
    if (filtertext === "") {
      setfilterdogs(alldoges);
    } else {
      const text = filtertext.toLowerCase();
      const fst = text.charAt(0);
      const upper = fst.toUpperCase();
      const lasttext = text.slice(1);
      const alltext = upper + lasttext;
      const filtering = alldoges.filter((item) => {
        return item.name === alltext;
      });
      setfilterdogs(filtering);
    }
  }, [filtertext]);

  const content = {
    filterdogs,
    filtertext,
    alldoges,
    setfiltertext,
    showWallet,
    setshowWallet,
    walletAddress,
    walletprice,
    setwalletAddress,
    setwalletwalletprice,
    connectHandler,
    disconnectHandler,
  };
  return (
    <Mycontext.Provider value={content}>{props.children}</Mycontext.Provider>
  );
};
export default Mycontext;
