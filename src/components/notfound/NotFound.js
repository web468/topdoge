import React from "react";

const NotFound = () => {
  return (
    <div className="notfound  d-flex align-center">
      <div>
        <h1>404</h1>
        <h5>this page is not found</h5>
      </div>
    </div>
  );
};

export default NotFound;
