import React, { useContext, useState, useEffect } from "react";
import "./SingleAnnemal.css";
import { useParams, useLocation } from "react-router-dom";
import Mycontext from "../../../store";
import { AiOutlineEye } from "react-icons/ai";
import WalletPhantom from "../../phantom/WalletPhantom";
import NotFound from "../../notfound/NotFound";
import img from "../../../Assets/Tag.png";
const SingleAnnemal = () => {
  const { pathname } = useLocation();
  const [PriceSol, setPriceSol] = useState(null);
  const param = useParams();
  const ctx = useContext(Mycontext);
  const [dogs, setdogs] = useState([]);

  // to scroll top
  useEffect(() => {
    window.scrollTo(0, 0);
  }, [pathname]);

  //filter method
  const index = dogs.findIndex((item) => {
    return item.address == param.address;
  });

  // fetch price from coin api
  useEffect(() => {
    const fetching2 = async () => {
      const res = await fetch("../../../../../dogs.json");
      const data = await res.json();
      setdogs(Object.values(data));
    };
    fetching2();
  }, []);
  useEffect(() => {
    const fetching2 = async () => {
      const res2 = await fetch(
        "https://rest.coinapi.io/v1/exchangerate/SOL/USD?apikey= 4CE94B8F-29A6-4DE2-8D11-1A3C15CAD930"
      );
      const data2 = await res2.json();
      setPriceSol(data2.rate);
    };
    fetching2();
  }, []);

  return (
    <>
      {index != -1 ? (
        <div className=" SingleAnnemal">
          <div className="container">
            <div className="row">
              <div className="col-12 col-md-6 ">
                <div className="align-center">
                  <div>
                    <div className="fst_img">
                      <img src={img} alt="" />
                    </div>
                    <div className="img">
                      <img src={dogs[index].image} alt="" />
                      <a
                        href={dogs[index].image}
                        className="outerbtn"
                        target="_blank"
                      >
                        <AiOutlineEye /> Preview
                      </a>
                    </div>
                    <h2 className="name d-none d-md-block mb-0">
                      {dogs[index].name}
                    </h2>
                    <p className="desc d-none d-md-block">
                      {dogs[index].description}
                    </p>
                  </div>
                </div>
              </div>
              <div className="col-12 col-md-6">
                <p className="desc mt-5 mt-md-0 mb-0 ">
                  {dogs[index].description}
                </p>
                <h1 className="name mb-4 mt-0">{dogs[index].name} </h1>
                <div className="row">
                  <div className="col-4 ">
                    <p className="desc mb-0">Royalties :</p>
                    <p className="fw-bold mb-0">
                      {dogs[index].seller_fee_basis_points / 100} %
                    </p>
                  </div>
                  <div className="col-8">
                    <p className="desc mb-0">view on :</p>
                    <a
                      className="link_blank fw-bold"
                      href={`https://explorer.solana.com/address/${dogs[index].address}?cluster=devnet`}
                      target="_blank"
                    >
                      solona
                    </a>
                  </div>
                </div>
                <hr className="m-0 my-2" />
                <div className="created_by ">
                  <p className="desc mb-0">Created By :</p>
                  {dogs[index].properties.creators.map((item, index) => (
                    <p className="fw-bold mb-0" key={index}>
                      address :
                      <a
                        target="_blank"
                        style={{ color: "black" }}
                        href={`https://explorer.solana.com/address/${item.address}?cluster=devnet`}
                      >
                        {item.address.slice(0, 4)} ...{item.address.slice(-4)}
                      </a>
                    </p>
                  ))}
                </div>
                <hr className="m-0 my-2" />
                <div className="row">
                  <div className="col-6">
                    <p className="desc mb-0">Edition : </p>
                    <p className="fw-bold mb-0"> NFT 0 </p>
                  </div>
                  <div className="col-6">
                    <p className="desc mb-0">Max Supply : </p>
                    <p className="fw-bold mb-0">
                      {dogs[index].properties.creators.length}
                    </p>
                  </div>
                </div>

                <hr className="m-0 my-2" />
                <div className="wallet">
                  <div className="price">
                    <div className="row">
                      <div className="col-12 ">
                        <p className="mb-0 desc">Price :</p>
                      </div>
                      <div className="col-6">
                        <p className="mb-0 fw-bold">{dogs[index].price}</p>
                      </div>
                      <div className="col-6">
                        <p className="fw-bold">
                          {(parseFloat(dogs[index].price) * PriceSol)
                            .toString()
                            .slice(0, 6)}
                          $
                        </p>
                      </div>
                    </div>
                  </div>
                  {!ctx.showWallet && <hr className="m-0 my-2" />}
                  {!ctx.showWallet && <WalletPhantom />}
                </div>
              </div>
            </div>
          </div>
        </div>
      ) : (
        <NotFound />
      )}
    </>
  );
};

export default React.memo(SingleAnnemal);
