import React, { useState, useEffect } from "react";
import "./items.css";
import { Card, Col, Row } from "react-bootstrap";
import { Link } from "react-router-dom";
import img from "../../../Assets/Tag.png";
const Items = () => {
  const [alldogs, setalldogs] = useState([]);
  useEffect(() => {
    const fetching = async () => {
      const res = await fetch("./dogs.json");
      const data = await res.json();
      setalldogs(Object.values(data));
    };
    fetching();
  }, []);
  return (
    <div className="container">
      <Row xs={1} sm={2} md={3} lg={4} className="g-4 mt-5">
        {alldogs.map((item, index) => (
          <Col key={index} className="mb-4">
            <Card>
              <Link to={`/marktplace/${item.address}`}>
                <div className="card_img">
                  <div className=" p-0 pl-3 mb-4 card_tag d-flex justify-content-start">
                    <img src={img} alt="" />
                  </div>
                  <div className="p-2 big_iimg d-flex align-items-center justify-content-center">
                    <img src={item.image} alt="" />
                  </div>
                </div>
                <div className="row m-0 card-body pt-2">
                  <div className="col-12 p-0">
                    <div className="row m-0">
                      <div className="col-6 pl-2 p-0">
                        <p className="mb-0 card_info">Release</p>
                      </div>
                      <div className="col-6 pl-4 p-0 text-center">
                        <p className="mb-0 card_info">{item.name}</p>
                      </div>
                    </div>
                    <div className="row m-0">
                      <div className="col-6 pl-2 p-0">
                        <p className="mb-0 card_info">Stamina</p>
                      </div>
                      <div className="col-6 pl-4 p-0  text-center">
                        <p className="mb-0 card_info">{item.symbol}</p>
                      </div>
                    </div>
                    <div className="row m-0 pb-2 dashed_line">
                      <div className="col-6 pl-2 p-0">
                        <p className="mb-0 card_info">Rarity</p>
                      </div>
                      <div className="col-6 pl-4 p-0 text-center">
                        <p className="mb-0 card_info">{item.symbol}</p>
                      </div>
                    </div>

                    <div className="row m-0 mt-2">
                      <div className="col-6 pl-2 p-0">
                        <p className="mb-0 card_info_total">Stats : </p>
                        <p className="last_text">{item.description}</p>
                      </div>
                      <div className="col-6 pl-4 p-0  text-center">
                        <p className="mb-0 card_info_total">Price :</p>
                        <p className="last_text">
                          ${item.seller_fee_basis_points}
                        </p>
                      </div>
                    </div>
                  </div>
                </div>
              </Link>
            </Card>
          </Col>
        ))}
      </Row>
    </div>
  );
};

export default React.memo(Items);
