import React from "react";
import "./FirstTopDoge.css";
import Wolf from "../../../Assets/Wolf.png";
const FirstTopDoge = () => {
  return (
    <div className="section-2">
      <div className="container inside_box ">
        <div className="inner_inside_box ">
          <div className="row">
            <div className="col-12 col-lg-7">
              <div className="inner_box">
                <div>
                  <div className=" text-center elements ">
                    <div className=" element">
                      <div className="">10</div>
                      <div className="">Days</div>
                    </div>
                    <div className=" element">
                      <div className="">21</div>
                      <div className="">Hours</div>
                    </div>
                    <div className=" element">
                      <div className="">12</div>
                      <div className="">Minutes</div>
                    </div>
                  </div>

                  <div className="count_heading">
                    <p>First topdoge collection coming soon!</p>
                  </div>

                  <div className="count_pera ">
                    <p>
                      Our first NFT collection of topdoges are launching soon.
                      Subscribe to our mailing list to reserve a free topdoge.
                    </p>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-12 col-lg-5">
              <div className="wolf_background d-flex align-items-center justify-content-center">
                <img src={Wolf} alt="wolf" />
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default FirstTopDoge;
