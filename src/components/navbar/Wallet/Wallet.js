import React, { useContext } from "react";
import "./wallet.css";
import { Dropdown } from "react-bootstrap";
import Mycontext from "../../../store";
const Wallet = () => {
  const ctx = useContext(Mycontext);
  console.log(ctx.walletAddress);
  return (
    <div className="wallet">
      <Dropdown>
        <Dropdown.Toggle className="walletbtn" id="dropdown-basic">
          Your Wallet
        </Dropdown.Toggle>

        <Dropdown.Menu className="p-2 py-3">
          <p className="mb-1">{ctx.walletprice && ctx.walletprice} SOL</p>
          <hr className="my-1" />
          <p className="mb-0">
            {ctx.walletAddress && (
              <a
                target="_blank"
                href={`https://explorer.solana.com/address/${ctx.walletAddress}?cluster=devnet`}
              >
                {ctx.walletAddress.slice(0, 22)} ...
              </a>
            )}
          </p>
          <button onClick={ctx.disconnectHandler}>Disconnect</button>
        </Dropdown.Menu>
      </Dropdown>
    </div>
  );
};

export default Wallet;
// https://explorer.solana.com/address/HDBWPx3xU7jQvWrCEQEKzsrNoJuqHpYcL1X3obYHB2wG?cluster=devnet
