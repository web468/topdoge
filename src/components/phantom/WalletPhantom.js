import React, { useContext } from "react";
import Mycontext from "../../store";

const PhantomConnect = () => {
  const ctx = useContext(Mycontext);

  return (
    <div>
      <button onClick={ctx.connectHandler}>Connect Your Phantom Wallet</button>
    </div>
  );
};

export default PhantomConnect;
